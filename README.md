# README #

We're still working on the Readme. Read the comments in the source code for now, along with the admin module descriptions.

### Setup ###

* Add the module code to sites/*/modules, as per usual
* Set the permissions after turning the module on
* go to the config page at admin/config/user-interface/zerohammer/admin to set the module up


### TODO ###

* Write the rest of the README
* Develop the logging system. Report URLs as domains per user per month i.e. domains posted repeatedly by one user / a few users go to the top.